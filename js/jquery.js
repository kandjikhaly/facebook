/*! jquery v3.6.0 | (c) openjs foundation and other contributors | jquery.org/license */
!function(e,t){"use strict";"object"==typeof module&&"object"==typeof
module.exports?module.exports=e.document?t(e,!0):function(e){if(!e.document)throw new
error("jquery requires a window with a document");return t(e)}:t(e)}("undefined"!=typeof
window?window:this,function(c,e){"use strict";var
t=[],r=object.getprototypeof,s=t.slice,g=t.flat?function(e){return t.flat.call(e)}:function(e){return
t.concat.apply([],e)},u=t.push,i=t.indexof,n={},o=n.tostring,v=n.hasownproperty,a=v.tostring,l=
a.call(object),y={},m=function(e){return"function"==typeof e&&"number"!=typeof
e.nodetype&&"function"!=typeof e.item},x=function(e){return
null!=e&&e===e.window},e=c.document,c={type:!0,src:!0,nonce:!0,nomodule:!0};function
b(e,t,n){var r,i,o=(n=n||e).createelement("script");if(o.text=e,t)for(r in
c)(i=t[r]||t.getattribute&&t.getattribute(r))&&o.setattribute(r,i);n.head.appendchild(o).parentnod
e.removechild(o)}function w(e){return null==e?e+"":"object"==typeof e||"function"==typeof
e?n[o.call(e)]||"object":typeof e}var f="3.6.0",s=function(e,t){return new s.fn.init(e,t)};function
p(e){var t=!!e&&"length"in
e&&e.length,n=w(e);return!m(e)&&!x(e)&&("array"===n||0===t||"number"==typeof
t&&0<t&&t-1 in e)}s.fn=s.prototype={jquery:f,constructor:s,length:0,toarray:function(){return
s.call(this)},get:function(e){return
null==e?s.call(this):e<0?this[e+this.length]:this[e]},pushstack:function(e){var
t=s.merge(this.constructor(),e);return t.prevobject=this,t},each:function(e){return
s.each(this,e)},map:function(n){return this.pushstack(s.map(this,function(e,t){return
n.call(e,t,e)}))},slice:function(){return
this.pushstack(s.apply(this,arguments))},first:function(){return this.eq(0)},last:function(){return
this.eq(-1)},even:function(){return
this.pushstack(s.grep(this,function(e,t){return(t+1)%2}))},odd:function(){return
this.pushstack(s.grep(this,function(e,t){return t%2}))},eq:function(e){var
t=this.length,n=+e+(e<0?t:0);return
this.pushstack(0<=n&&n<t?[this[n]]:[])},end:function(){return
this.prevobject||this.constructor()},push:u,sort:t.sort,splice:t.splice},s.extend=s.fn.extend=func
tion(){var e,t,n,r,i,o,a=arguments[0]||{},s=1,u=arguments.length,l=!1;for("boolean"==typeof
a&&(l=a,a=arguments[s]||{},s++),"object"==typeof
a||m(a)||(a={}),s===u&&(a=this,s--);s<u;s++)if(null!=(e=arguments[s]))for(t in
e)r=e[t],"__proto__"!==t&&a!==r&&(l&&r&&(s.isplainobject(r)||(i=array.isarray(r)))?(n=a[t],o=i
&&!array.isarray(n)?[]:i||s.isplainobject(n)?n:{},i=!1,a[t]=s.extend(l,o,r)):void
0!==r&&(a[t]=r));return
a},s.extend({expando:"jquery"+(f+math.random()).replace(/\d/g,""),isready:!0,error:function(e)
{throw new error(e)},noop:function(){},isplainobject:function(e){var t,n;return!(!e||"[object
object]"!==o.call(e))&&(!(t=r(e))||"function"==typeof(n=v.call(t,"constructor")&&t.constructor)&
&a.call(n)===l)},isemptyobject:function(e){var t;for(t in
e)return!1;return!0},globaleval:function(e,t,n){b(e,{nonce:t&&t.nonce},n)},each:function(e,t){v
ar n,r=0;if(p(e)){for(n=e.length;r<n;r++)if(!1===t.call(e[r],r,e[r]))break}else for(r in
e)if(!1===t.call(e[r],r,e[r]))break;return e},makearray:function(e,t){var n=t||[];return
null!=e&&(p(object(e))?s.merge(n,"string"==typeof
e?[e]:e):u.call(n,e)),n},inarray:function(e,t,n){return
null==t?-1:i.call(t,e,n)},merge:function(e,t){for(var
n=+t.length,r=0,i=e.length;r<n;r++)e[i++]=t[r];return e.length=i,e},grep:function(e,t,n){for(var
r=[],i=0,o=e.length,a=!n;i<o;i++)!t(e[i],i)!==a&&r.push(e[i]);return r},map:function(e,t,n){var
r,i,o=0,a=[];if(p(e))for(r=e.length;o<r;o++)null!=(i=t(e[o],o,n))&&a.push(i);else for(o in